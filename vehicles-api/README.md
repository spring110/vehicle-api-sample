# Vehicles API

A REST API to maintain vehicle data and to provide a complete
view of vehicle details including price and address.

## Instructions

- First run the eureka-server, pricing-service, boogle-maps applications.
- Then run the VehiclesApiApplication.java file in this application.
- You can then send CRUD requests to this vehicle-api to create a vehicle, update a vehicle, get a vehicle, and delete a vehicle.

### Retrieve a Vehicle

`GET` `http://localhost:8080/cars/{id}`

This request retrieves the vehicle data from the database.
The Pricing Service and Boogle Maps are used to get the
vehicle price information and the address information.

### Create a Vehicle

In order to create a vehicle, please send a POST request like in the below:

`POST` `http://localhost:8080/cars`
```json
{
   "condition":"USED",
   "details":{
      "body":"sedan",
      "model":"Impala",
      "manufacturer":{
         "code":101,
         "name":"Chevrolet"
      },
      "numberOfDoors":4,
      "fuelType":"Gasoline",
      "engine":"3.6L V6",
      "mileage":32280,
      "modelYear":2018,
      "productionYear":2018,
      "externalColor":"white"
   },
   "location":{
      "lat":40.73061,
      "lon":-73.935242
   }
}
```

For the condition, please use NEW or USED.

For the manufacturer information, please choose among the following:
100, "Audi"
101, "Chevrolet"
102, "Ford"
103, "BMW"
104, "Dodge"

Based on the location lattitude ang longitude, system will assign an address to this vehicle.
System will also assign a pricing to this vehicle using the pricing-service.

### Update a Vehicle

In order to update an existing vehicle, please send a PUT request like in the below:

`PUT` `http://localhost:8080/cars/{id}`

```json
{
   "condition":"USED",
   "details":{
      "body":"sedan",
      "model":"Impala",
      "manufacturer":{
         "code":101,
         "name":"Chevrolet"
      },
      "numberOfDoors":4,
      "fuelType":"Gasoline",
      "engine":"3.6L V6",
      "mileage":32280,
      "modelYear":2018,
      "productionYear":2018,
      "externalColor":"white"
   },
   "location":{
      "lat":57.577198,
      "lon":62.227851
   }
}
```

### Delete a Vehicle

`DELETE` `http://localhost:8080/cars/{id}`
