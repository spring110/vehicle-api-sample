# Boogle Maps

This is a Mock that simulates a Maps WebService where, given a latitude
longitude, will return a random address.

## Instructions
- Run the BoogleMapsApplication.java file.
- Type http://localhost:9191/maps?lat={NUMBER}&lon={NUMBER} in the browser.
- The API will return a random address every time you make a query.