# Pricing Service

The Pricing Service is a REST WebService that simulates a backend that
would give the price of a vehicle given a vehicle id as
input.

## Instructions
- First run the eureka-server application since Pricing Service should be run on the Eureka service.
- Run the PricingServiceApplication.java file.
- Type http://localhost:8082/services/price?vehicleId={NUMBER} in your browser.
- There are 19 vehicle prices in the mock database. Please choose a number between 1-19.
